def mySqrt(x):
    if x == 0:
        return 0
    k = 1.0
    while abs(k*k-x) >= 1:
        k = (k+x/k)/2
    return int(k)

if __name__ == "__main__":
    
    example1 = mySqrt(4)
    example2 = mySqrt(8)
    print(example1)
    print(example2)