def twoSum(nums, target):
    lookup = {}
    for cnt, num in enumerate(nums):
        if target - num in lookup:
            return lookup[target-num], cnt
        lookup[num] = cnt 

if __name__ == "__main__":
    example1 = twoSum([2,7,11,15], 9)
    example2 = twoSum([3,2,4], 6)
    example3 = twoSum([3,3], 6)
    print(example1)
    print(example2)
    print(example3)